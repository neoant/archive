= Archive
John Doe <iocdr@protonmail.com>

image:https://img.shields.io/badge/chat-neoant:matrix.org-brightgreen[
    link="https://matrix.to/#/#neoant:matrix.org"]

The NeoAnt Project aims to develop a modern operating system. It plans to
accomplish this goal by combining cutting-edge research in Operating Systems,
Security, and Programming Languages. All software released under The NeoAnt
Project will respect the user's freedom according to the terms of the Affero
General Public License (AGPLv3+).

== How can we make a better operating system?

The only way to build a modern operating system is to rethink how operating
systems are designed. A few key features of such an architecture are detailed
below:

* Software Isolated Processes
+
Software isolated processes provide lightweight protection domains that are
established by language safety rules and enforced by static type checking.

* Capability-based Security
+
Capability-based security allows precise, minimal, and meaningful delegation of
authority, which is fundamental to the secure operation of a system.

* Communication using Message Passing
+
Message passing provides a much more scalable solution when compared to shared
memory models.

* Minimal Trusted Computing Base
+
In order to reduce costs and security risks, the Trusted Computing Base should
be kept minimal.

== Related work

* Liedtke, Jochen. "On µ-kernel construction." Proceedings of the 15th ACM Symposium on OS Principles. 1995.
* Bershad, Brian N., et al. "Extensibility safety and performance in the SPIN operating system." ACM SIGOPS Operating Systems Review 29.5 (1995): 267-283.
* Bershad, Brian N., et al. "Protection is a software issue." Proceedings 5th Workshop on Hot Topics in Operating Systems (HotOS-V). IEEE, 1995.
* Shapiro, Jonathan S., Jonathan M. Smith, and David J. Farber. EROS: a fast capability system. Vol. 33. No. 5. ACM, 1999.
* Miller, Mark S., Ka-Ping Yee, and Jonathan Shapiro. Capability myths demolished. Vol. 5. Technical Report SRL2003-02, Johns Hopkins University Systems Research Laboratory, 2003. http://www.erights.org/elib/capability/duals, 2003.
* Hunt, Galen C., and James R. Larus. "Singularity: rethinking the software stack." ACM SIGOPS Operating Systems Review 41.2 (2007): 37-49.
* Fähndrich, Manuel, et al. "Language support for fast and reliable message-based communication in Singularity OS." ACM SIGOPS Operating Systems Review. Vol. 40. No. 4. ACM, 2006.
* Aiken, Mark, et al. "Deconstructing process isolation." Proceedings of the 2006 workshop on Memory system performance and correctness. ACM, 2006.
* Baumann, Andrew, et al. "The multikernel: a new OS architecture for scalable multicore systems." Proceedings of the ACM SIGOPS 22nd symposium on Operating systems principles. ACM, 2009.
* Duffy, Joe. "Blogging about Midori." Joe Duffy's Blog. November 3, 2015.

== Version Control

* Jacobson, Judah. "A formalization of darcs patch theory using inverse
  semigroups." Available from ftp://ftp.math.ucla.edu/pub/camreport/cam09-83.
  pdf (2009).
* Mimram, Samuel, and Cinzia Di Giusto. "A categorical theory of patches."
  Electronic notes in theoretical computer science 298 (2013): 283-307.
* Angiuli, Carlo, et al. "Homotopical patch theory." ACM SIGPLAN Notices. Vol.
  49. No. 9. ACM, 2014.

== Shell

* Moore, Scott, et al. "SHILL: A Secure Shell Scripting Language." 11th USENIX
  Symposium on Operating Systems Design and Implementation (OSDI 14). 2014.

== File System

* Jannen, William, et al. "BetrFS: A right-optimized write-optimized file
  system." 13th USENIX Conference on File and Storage Technologies (FAST 15).
  2015.
* Yuan, Jun, et al. "Optimizing every operation in a write-optimized file
  system." 14th USENIX Conference on File and Storage Technologies (FAST 16).
  2016.
* Conway, Alex, et al. "File systems fated for senescence? nonsense, says
  science!." 15th USENIX Conference on File and Storage Technologies (FAST 17).
  2017.
* Zhan, Yang, et al. "The full path to full-path indexing." 16th USENIX
  Conference on File and Storage Technologies (FAST 18). 2018.

== Compiler Collection

* Lattner, Chris Arthur. LLVM: An infrastructure for multi-stage optimization.
  Diss. University of Illinois at Urbana-Champaign, 2002.
* Appel, Andrew W. Compiling with continuations. Cambridge University Press,
  2006.
* Brünnler, Kai, and Richard McKinley. "An algorithmic interpretation of a deep
  inference system." International Conference on Logic for Programming
  Artificial Intelligence and Reasoning. Springer, Berlin, Heidelberg, 2008.
* Harris, William R., et al. "Declarative, temporal, and practical programming
  with capabilities." 2013 IEEE Symposium on Security and Privacy. IEEE, 2013.
* Leißa, Roland, Marcel Köster, and Sebastian Hack. "A graph-based higher-order
  intermediate representation." Proceedings of the 13th Annual IEEE/ACM
  International Symposium on Code Generation and Optimization. IEEE Computer
  Society, 2015.
* Downen, Paul, et al. "Sequent calculus as a compiler intermediate language."
  ACM SIGPLAN Notices. Vol. 51. No. 9. ACM, 2016.
* Acar, Umut A., et al. "Dag-calculus: a calculus for parallel computation." ACM
  SIGPLAN Notices. Vol. 51. No. 9. ACM, 2016.
* Jung, Ralf, et al. "RustBelt: Securing the foundations of the Rust programming
  language." Proceedings of the ACM on Programming Languages 2.POPL (2017): 66.
* Lattner, Chris, and Jacques Pienaar. "MLIR Primer: A Compiler Infrastructure
  for the End of Moore’s Law." (2019).
* Cong, Youyou, et al. "Compiling with Continuations, or without? Whatever."
  Proceedings of the ACM on Programming Languages 3.ICFP (2019): 79.

== Allocator

* Berger, Emery D., et al. "Hoard: A scalable memory allocator for multithreaded
  applications." ACM SIGARCH Computer Architecture News. Vol. 28. No. 5. ACM,
  2000.
* Evans, Jason. "A scalable concurrent malloc (3) implementation for FreeBSD."
  Proc. of the bsdcan conference, ottawa, canada. 2006.

